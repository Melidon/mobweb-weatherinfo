package hu.bme.aut.android.weatherinfo.feature.city

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import hu.bme.aut.android.weatherinfo.R
import hu.bme.aut.android.weatherinfo.databinding.ActivityCityBinding
import hu.bme.aut.android.weatherinfo.feature.details.DetailsActivity

class CityActivity : AppCompatActivity(), CityAdapter.OnCitySelectedListener,
    AddCityDialogFragment.AddCityDialogListener {

    private lateinit var binding: ActivityCityBinding
    private lateinit var adapter: CityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initFab()
        initRecyclerView()
    }

    private fun initFab() {
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            AddCityDialogFragment().show(
                supportFragmentManager,
                AddCityDialogFragment::class.java.simpleName
            )
        }
    }

    private fun initRecyclerView() {
        binding.content.MainRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = CityAdapter(this)
        adapter.addCity("Budapest")
        adapter.addCity("Debrecen")
        adapter.addCity("Sopron")
        adapter.addCity("Szeged")
        binding.content.MainRecyclerView.adapter = adapter
    }

    override fun onCitySelected(city: String?) {
        val showDetailsIntent = Intent()
        showDetailsIntent.setClass(this@CityActivity, DetailsActivity::class.java)
        showDetailsIntent.putExtra(DetailsActivity.EXTRA_CITY_NAME, city)
        startActivity(showDetailsIntent)
    }

    override fun onCityAdded(city: String) {
        adapter.addCity(city)
    }

    override fun removeCity(position: Int) {
        adapter.removeCity(position)
    }

}

