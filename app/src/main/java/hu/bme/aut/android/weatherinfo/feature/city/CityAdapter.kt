package hu.bme.aut.android.weatherinfo.feature.city

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.weatherinfo.R

class CityAdapter internal constructor(private val listener: OnCitySelectedListener?) :
    RecyclerView.Adapter<CityAdapter.CityViewHolder>() {

    private val cities: MutableList<String>

    interface OnCitySelectedListener {
        fun onCitySelected(city: String?)
        fun removeCity(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_city, parent, false)
        return CityViewHolder(view)
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        val item = cities[position]
        holder.nameTextView.text = cities[position]
        holder.item = item
    }

    override fun getItemCount(): Int {
        return cities.size
    }

    fun addCity(newCity: String) {
        cities.add(newCity)
        notifyItemInserted(cities.size - 1)
    }

    fun removeCity(position: Int) {
        cities.removeAt(position)
        notifyItemRemoved(position)
        if (position < cities.size) {
            notifyItemRangeChanged(position, cities.size - position)
        }
    }

    inner class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val nameTextView: TextView = itemView.findViewById(R.id.CityItemNameTextView)
        val removeButton: Button = itemView.findViewById(R.id.CityItemRemoveButton)

        var item: String? = null

        init {
            itemView.setOnClickListener { listener?.onCitySelected(item) }
            removeButton.setOnClickListener { listener?.removeCity(cities.indexOf(item)) }
        }
    }

    init {
        cities = ArrayList()
    }
}
