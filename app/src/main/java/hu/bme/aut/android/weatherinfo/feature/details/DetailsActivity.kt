package hu.bme.aut.android.weatherinfo.feature.details

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.weatherinfo.R
import hu.bme.aut.android.weatherinfo.databinding.ActivityDetailsBinding
import hu.bme.aut.android.weatherinfo.model.WeatherData
import hu.bme.aut.android.weatherinfo.network.NetworkManager

class DetailsActivity : AppCompatActivity(), WeatherDataHolder {

    private lateinit var binding: ActivityDetailsBinding

    private var city: String? = null
    private var weatherData: WeatherData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        city = intent.getStringExtra(EXTRA_CITY_NAME)
        supportActionBar?.title = getString(R.string.weather, city)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onResume() {
        super.onResume()
        loadWeatherData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getWeatherData(): WeatherData? {
        return weatherData
    }

    private fun loadWeatherData() {
        NetworkManager.getWeather(city, ::displayWeatherData, ::showError)
    }

    private fun displayWeatherData(receivedWeatherData: WeatherData) {
        weatherData = receivedWeatherData
        val detailsPagerAdapter = DetailsPagerAdapter(supportFragmentManager, this)
        binding.mainViewPager.adapter = detailsPagerAdapter
    }

    private fun showError(throwable: Throwable) {
        throwable.printStackTrace()
        Toast.makeText(
            this,
            "Network request error occurred, check LOG",
            Toast.LENGTH_SHORT
        ).show()
    }


    companion object {
        private const val TAG = "DetailsActivity"
        const val EXTRA_CITY_NAME = "extra.city_name"
    }
}

