package hu.bme.aut.android.weatherinfo.model

data class Coord (
    var lon: Float,
    var lat: Float
)
