package hu.bme.aut.android.weatherinfo.model

data class Sys (
    var type: Int,
    var id: Int,
    var country: String,
    var sunrise: Int,
    var sunset: Int
)
