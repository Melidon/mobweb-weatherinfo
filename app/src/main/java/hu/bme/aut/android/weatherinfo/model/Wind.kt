package hu.bme.aut.android.weatherinfo.model

class Wind(
    val speed: Float,
    val deg: Float
)
